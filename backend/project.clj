(defproject maxit "0.1.0-SNAPSHOT"
  :description "Backend for MaxIt"
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main ^:skip-aot maxit.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
