var gGameOptions = {
    // Number of rows and columns in the game board.
    rows: 10,
    cols: 10,
    // Default number of seconds to show a message.
    showMessageLength: 30,
    // Default maximum and minimum values that can appear on the game board.
    maxValue: 15,
    minValue: -15,
    // Computer level.
    computerLevel: 2,
    // Opponent.
    opponent: "computer",
    // Whether or not to keep the win/loss record.
    keepWL: "0"
};

var boardSizeChoices = [5, 7, 10, 15];
var computerLevelChoices = [0, 1, 2];
var opponentType = ['human', 'computer'];

// These will be two dimensional arrays that are row-major indexed
// (i.e. gGameBoard[row][col], not gGameBoard[col][row]).
var gGameBoard = [];
var gGameBoardVisited = [];

// The overall state of the current game.
var gGame = {
    state: "",
    selRow: -1,
    selCol: -1,
    gameOver: false
};

gCookieName = "SBSMaxIt";

function Player(n, c) {
    this.score = 0;
    this.name = n;
    this.isComputer = (c == null ? false : c);
    this.lastMove = {};
}

function Point(x, y) {
    this.x = x;
    this.y = y;
    this.toString = function() {
        return (this.x + "_" + this.y);
    };
    this.fromString = function(str) {
        var s = str.split("_");
        this.x = parseInt(s[0]);
        this.y = parseInt(s[1]);
        return (this);
    };
}
/**
 * This class is used by the MinMax search.  It is the nodes of the search tree.
 */
function Node(p) {
    // Reference to the parent node.
    this.parent = p;
    // A Point object telling what cell this node references.
    this.cell = new Point(0, 0);
    // An array of choices the next player can make.
    this.children = new Array(gGameOptions.rows);
    // The value of this node.
    this.value = -99;
}


var gPlayer1 = new Player(null);
var gPlayer2 = new Player(null, true);

var gMinMaxSearch = {
    root: new Node(null),
    humanScore: 0,
    computerScore: 0,
    // Indexed by gGameOptions.computerLevel (i.e. a level 3 computer will
    // have a maximum search depth of 7).
    maxSearchDepth: [1, 3, 5, 7, 9, 11, 13],
    curSearchDepth: 0,
    getMaxSearchDepth: function() {
        if( gGameOptions.computerLevel >= this.maxSearchDepth.length ) {
            gGameOptions.computerLevel = this.maxSearchDepth.length - 1;
        }
        return ( this.maxSearchDepth[gGameOptions.computerLevel] );
    },
    visited: []
};

//noinspection JSUnusedGlobalSymbols
function zeroPadNumbers(num, numDigits) {
    if( numDigits == null ) {
        numDigits = 2;
    }
    var powerOfTen = Math.pow(10, numDigits - 1);
    if( num < powerOfTen ) {
        return (num);
    }
    var numZeros = ("" + (num % powerOfTen)).length;
    var zeros = "";
    for( var i = 0; i < numZeros; i++ ) {
        zeros += "0";
    }
    return ( zeros + "" + num );
}

function verifyBrowserSize() {
    var winWidth = $(window).width();
    var winHeight = $(window).height();
    //if( winWidth < 600 || winHeight < 400 ) {
    //    throw "The browser window is too small ("+winWidth+","+winHeight+").  The browser must be at least 800x600 to display MaxIt.";
    //}
    return ([winWidth, winHeight]);
}

function calculateGameBoardDigitFontSize(cellWidth) {
    return ( parseInt(cellWidth / 3) );
}

function buildGameBoard() {
    var browserSize = verifyBrowserSize();
    var gbHTML = "";
    var tableSize = Math.min(browserSize[0] * 0.90, browserSize[1] - $("#divTitlebar").height() - $("#tableScoreboard").height() - 35);
    var cellWidth = parseInt(tableSize / gGameOptions.cols);
    var fontSize = calculateGameBoardDigitFontSize(cellWidth);
    for( var j = 0; j < gGameOptions.rows; j++ ) {
        gbHTML += "<TR id=\"trGameBoard" + j + "\">";
        for( var i = 0; i < gGameOptions.cols; i++ ) {
            var p = new Point(i, j);
            gbHTML += "<TD id=\"tdGameBoard" + p.toString() + "\">" + i + "</TD>";
        }
        gbHTML += "</TR>";
    }
    $("#tableGameBoard").html(gbHTML);
    $("table#tableGameBoard td")
        .css("width", cellWidth)
        .css("height", cellWidth)
        .css("font-size", fontSize + "pt")
    ;
}
function fillGameBoard() {
    var val = 0;
    var rndMultiplier = gGameOptions.maxValue * 2;
    gGameBoard = [];
    gGameBoardVisited = [];
    // Create the grid.  Each cell can have a random number between
    //  -15 and 14 (inclusive) except for a 0.  Cells cannot have
    //  a 0 value.
    for( var j = 0; j < gGameOptions.rows; j++ ) {
        gGameBoard[j] = [];
        gGameBoardVisited[j] = [];
        for( var i = 0; i < gGameOptions.cols; i++ ) {
            do {
                val = Math.floor(Math.random() * (rndMultiplier)) - gGameOptions.maxValue;
            } while( val == 0 );
            gGameBoard[j].push(val);
            gGameBoardVisited[j].push(false);
            var p = new Point(i, j);
            var gbCellID = "tdGameBoard" + p.toString();
            $("#" + gbCellID).html(val);
        }
    }
}
function updateScoreboard() {
    var player1NameElem = $("#player1Name");
    var player2NameElem = $("#player2Name");

    player1NameElem.css("color", "black").html(gPlayer1.name);
    player2NameElem.css("color", "black").html(gPlayer2.name);

    $("#player1Score").html(gPlayer1.score);
    $("#player2Score").html(gPlayer2.score);

    if( gPlayer1.lastMove.value != null ) {
//		$("#spanPlayer1LastMove").html(gPlayer1.lastMove.value+" @ "+gPlayer1.lastMove.coords.toString());
    }
    if( gPlayer2.lastMove.value != null ) {
//		$("#spanPlayer2LastMove").html(gPlayer2.lastMove.value+" @ "+gPlayer2.lastMove.coords.toString());
    }

    // Set UI elements to denote which player's turn it is.
    if( gGame.state == "player1turn" ) {
        player1NameElem.css("color", "blue").html("* " + gPlayer1.name);
    }
    else if( gGame.state == "player2turn" ) {
        player2NameElem.css("color", "blue").html("* " + gPlayer2.name);
    }
}

function resetGameState() {
    // Reset the game state.
    gGame = {
        state: "",
        selRow: -1,
        selCol: -1,
        gameOver: false
    };
    // Reset the scores.
    gPlayer1.score = gPlayer2.score = 0;
}

function setPlayerNames(p1, p2) {
    // If the name is null then the player's name has not been set, so we set
    // it for them.  If it is not null then it has already been set and we keep
    // that name.
    if( gPlayer1.name == null ) {
        gPlayer1.name = p1;
    }
    if( gPlayer2.name == null ) {
        gPlayer2.name = p2;
    }
}

function newGame() {
    try {
        loadGameOptions();
        resetGameState();
        buildGameBoard();
        fillGameBoard();

        // Set up the default names of the players.
        switch( gGameOptions.opponent ) {
            case "computer":
                setPlayerNames("Human", "Computer");
                break;
            case "human":
            case "remote_human":
                setPlayerNames("Player 1", "Player 2");
                // This is to make sure no players are marked as a computer.
                gPlayer2.isComputer = gPlayer1.isComputer = false;
                break;
        }

        // Set the state of the game to player 1's turn.
        gGame.state = "player1turn";

        updateScoreboard();

        // Start the game.
        nextTurn();
    } catch( err ) {
        showMsg("Error creating new Game: " + err, "error", 10);
    }
}

function setBoardColors() {
    var color = "";
    for( var i = 0; i < gGameOptions.cols; i++ ) {
        for( var j = 0; j < gGameOptions.rows; j++ ) {
            var p = new Point(i, j);
            var gameBoardCell = $("#tdGameBoard" + p.toString());

            // Default the square to white.
            gameBoardCell.css("background-color", "white");

            // Mark the current row/col as yellow (or lightgrey if the player is a computer).
            if( gGame.state == "player1turn" && j == gGame.selRow ) {
                color = "yellow";
                if( gPlayer1.isComputer ) {
                    color = "lightgrey";
                }
                gameBoardCell.css("background-color", color);
            }
            else if( gGame.state == "player2turn" && i == gGame.selCol ) {
                color = "yellow";
                if( gPlayer2.isComputer ) {
                    color = "lightgrey";
                }
                gameBoardCell.css("background-color", color);
            }

            // Mark visited squares as red.
            if( gGameBoardVisited[j][i] == true ) {
                gameBoardCell.css("background-color", "red");
            }
        }
    }
}

function updateUI() {
    updateScoreboard();
    setBoardColors();
}

function isComputerTurn() {
    return ( (gGame.state == "player1turn" && gPlayer1.isComputer) ||
    (gGame.state == "player2turn" && gPlayer2.isComputer) );
}

function markVisited(p) {
    gGameBoardVisited[p.y][p.x] = true;
    // Color the cell red to show it has already been chosen.
    $("#tdGameBoard" + p.toString()).css("background-color", "red");
}

function nextPlayerTurn(val, coords) {
    if( gGame.state == "player1turn" ) {
        gPlayer1.lastMove = {
            value: val,
            coords: coords
        };
        return ("player2turn");
    }
    if( gGame.state == "player2turn" ) {
        gPlayer2.lastMove = {
            value: val,
            coords: coords
        };
        return ("player1turn");
    }
}

//noinspection JSUnusedGlobalSymbols
function getPositionOfMaxInCol(col) {
    var maxPos = 0;
    var j;

    // Find the first unused square.
    for( j = 0; j < gGameOptions.rows; j++ ) {
        if( gGameBoardVisited[j][col] == false ) {
            maxPos = j;
            break;
        }
    }
    // Find the largest value in the column.  We start at 'maxPos' because
    //  we've already determined (with the previous for() loop) that all
    //  positions above 'maxPos' in the column have already been selected.
    for( j = maxPos; j < gGameOptions.rows; j++ ) {
        if( gGameBoardVisited[j][col] == false && gGameBoard[j][col] > gGameBoard[maxPos][col] ) {
            maxPos = j;
        }
    }
    return (maxPos);
}

function computerMove() {
    var j, maxPos;

    // Setup the 'visited' board so that the computer can track
    // what he is doing while he searches.
    gMinMaxSearch.visited = new Array(gGameOptions.rows);
    for( j = 0; j < gGameOptions.rows; j++ ) {
        gMinMaxSearch.visited[j] = new Array(gGameOptions.cols);
        for( var i = 0; i < gGameOptions.cols; i++ ) {
            gMinMaxSearch.visited[j][i] = gGameBoardVisited[j][i];
        }
    }
    // The current search depth the computer is at.
    gMinMaxSearch.curSearchDepth = 0;
    // We keep track of the scores of each player as the computer
    // searches future moves and use them as an evaluation of
    // the moves (the farther ahead the computer is, the better the move).
    gMinMaxSearch.humanScore = window[getHumanPlayer()].score;
    gMinMaxSearch.computerScore = window[getComputerPlayer()].score;

    // If 'selRow' is -1 then the computer is making the first
    // move of the game.
    if( gGame.selRow == -1 ) {
    }
    else {
        gMinMaxSearch.root.cell.x = gGame.selCol;
        gMinMaxSearch.root.cell.y = gGame.selRow;
        gMinMaxSearch.root.parent = null;
        // Given a column to look in, the computer must choose which cell
        // in the column will be the best move.
        for( j = 0; j < gGameOptions.rows; j++ ) {
            // If the cell has already been visited, we can't use it.
            if( gMinMaxSearch.visited[j][gMinMaxSearch.root.cell.x] == true || j == gMinMaxSearch.root.cell.y ) {
                gMinMaxSearch.root.children[j] = true;
                continue;
            }
            var nextNode = new Node(gMinMaxSearch.root);
            nextNode.cell.x = gMinMaxSearch.root.cell.x;
            nextNode.cell.y = j;
            gMinMaxSearch.computerScore += gGameBoard[nextNode.cell.y][nextNode.cell.x];
            gMinMaxSearch.root.children[j] = minmax(nextNode, "computerturn");
        }
        var result = maxVal(gMinMaxSearch.root.children, gGameOptions.rows);

        if( result != -1 ) {
            maxPos = result;
        }
    }
    // The computer has chosen a move, here we actually make the move.
    window[getComputerPlayer()].score += gGameBoard[maxPos][gGame.selCol];
    var p = new Point(gGame.selCol, maxPos);
    markVisited(p);
    gGame.selRow = maxPos;
    gGame.state = nextPlayerTurn(gGameBoard[maxPos][gGame.selCol], p);
    nextTurn();
}

/**
 * The evaluation function.  This functions determines the value
 *  of each node in the MinMax search tree.
 */
function evaluate() {
    return ( gMinMaxSearch.computerScore - gMinMaxSearch.humanScore );
}

/**
 * Returns the array index of the smallest value in the 'arr' array.
 */
function minVal(arr, size) {
    var minPos = -1;
    var unusable = true;

    for( var q = 0; q < size; q++ ) {
        if( arr[q] != true && minPos == -1 ) {
            unusable = false;
            minPos = q;
        }
    }
    if( unusable ) {
        return (-1);
    }

    for( var i = 0; i < size; i++ ) {
        if( arr[i] == true ) {
            continue;
        }
        if( arr[i] < arr[minPos] ) {
            minPos = i;
        }
    }

    return (minPos);
}

/**
 * Returns the largest value in the 'arr' array.
 */
function maxVal(arr, size) {
    var maxPos = -1;
    var unusable = true;

    // Check to see if the array is filled with only ALREADYUSED values.
    //  At the same time we set 'maxPos' to the smallest index that does
    //  not have an ALREADYUSED value.  This way we insure that the
    //  'array[maxPos]' has a value at the start of the next for() loop.
    for( var q = 0; q < size; q++ ) {
        if( arr[q] != true && maxPos == -1 ) {
            unusable = false;
            maxPos = q;
        }
    }
    if( unusable ) {
        return (-1);
    }

    // Find the array position with the largest value.
    for( var i = 0; i < size; i++ ) {
        if( arr[i] == true ) {
            continue;
        }
        if( arr[i] > arr[maxPos] ) {
            maxPos = i;
        }
    }

    return (maxPos);
}

/**
 * This function performs a MinMax search using the 'startNode'
 *  as the root.  It is called recursively to traverse the tree
 *  in a depth first fashion.  When each node has had all it's
 *  children traversed it chooses either the largest value or the
 *  smallest value, depending on whose turn the node represents.
 */
function minmax(startNode, turn) {
    var nextNode = new Node();
    var result;

    // Keep track of the depth we are currently at.
    gMinMaxSearch.curSearchDepth++;
    // Computer turns are MAX levels.  This section (even though it is labeled
    // "computer turn" is where we select the human's next move.
    if( turn == "computerturn" ) {
        // When we've reached the "bottom" of the tree we assign that node
        //  a value and move back up to it's parent.  The parent stores the
        //  value of it's children.  To back up we need to undo what we did
        //  to get to this node (so we change the score and the visited[][] array).
        if( gMinMaxSearch.curSearchDepth == gMinMaxSearch.getMaxSearchDepth() ) {
            gMinMaxSearch.curSearchDepth--;
            result = evaluate();
            gMinMaxSearch.computerScore -= gGameBoard[startNode.cell.y][startNode.cell.x];
            gMinMaxSearch.visited[startNode.cell.y][startNode.cell.x] = gGameBoardVisited[startNode.cell.y][startNode.cell.x];
            return (result);
        }
        // Mark that we've been here.
        gMinMaxSearch.visited[startNode.cell.y][startNode.cell.x] = true;
        // We iterate through all the possible moves of the human given
        // that the computer just chose the 'startNode' position.
        for( var x = 0; x < gGameOptions.cols; x++ ) {
            // We don't look at cells that have already been used.
            if( gMinMaxSearch.visited[startNode.cell.y][x] == true || x == startNode.cell.x ) {
                startNode.children[x] = true;
                continue;
            }
            nextNode.parent = startNode;
            nextNode.cell.x = x;
            nextNode.cell.y = startNode.cell.y;
            gMinMaxSearch.humanScore += gGameBoard[nextNode.cell.y][nextNode.cell.x];
            startNode.children[x] = minmax(nextNode, "humanturn");
        }
        // The computer has just (hypothetically) moved to the 'startNode'
        //  position, so all of the 'startNode''s children are human moves.
        //  Because we assume that the human will make the best move we
        //  choose the smallest value of the children (the value is the
        //  computer's score subtracted from the human's score).  So small
        //  values are bad for the computer and good for the human.
        result = minVal(startNode.children, gGameOptions.rows);
        gMinMaxSearch.computerScore -= gGameBoard[startNode.cell.y][startNode.cell.x];
        // 'result' is the position in the '.children' array of the best
        //  move.  If it is -1 (an impossible value for an array index)
        //  we know something is wrong.
        if( result == -1 ) {
            if( gMinMaxSearch.computerScore > gMinMaxSearch.humanScore ) {
                return (false);
            }
            // We reached a point where there are no more moves that can be
            // made so we evaluate the node as if we had reached our maximum
            // search depth.
            gMinMaxSearch.curSearchDepth--;
            result = evaluate();
            gMinMaxSearch.computerScore -= gGameBoard[startNode.cell.y][startNode.cell.x];
            gMinMaxSearch.visited[startNode.cell.y][startNode.cell.x] = gGameBoardVisited[startNode.cell.y][startNode.cell.x];
            return (result);
//			return(true);
        }
    }
    // Human turns are MIN levels.
    else {
        // When we've reached the "bottom" of the tree we assign that node
        //  a value and move back up to it's parent.  The parent stores the
        //  value of it's children.  To back up we need to undo what we did
        //  to get to this node (so we change the score and the visited[][] array).
        if( gMinMaxSearch.curSearchDepth == gMinMaxSearch.getMaxSearchDepth() ) {
            gMinMaxSearch.curSearchDepth--;
            result = evaluate();
            gMinMaxSearch.humanScore -= gGameBoard[startNode.cell.y][startNode.cell.x];
            gMinMaxSearch.visited[startNode.cell.y][startNode.cell.x] = gGameBoardVisited[startNode.cell.y][startNode.cell.x];
            return (result);
        }
        // Mark that we've been here.
        gMinMaxSearch.visited[startNode.cell.y][startNode.cell.x] = true;
        // We iterate through all the possible moves of the computer given
        //  that the human just chose the 'startNode' position.
        for( var y = 0; y < gGameOptions.rows; y++ ) {
            // We don't look at cells that have already been used.
            if( gMinMaxSearch.visited[y][startNode.cell.x] == true || y == startNode.cell.y ) {
                startNode.children[y] = true;
                continue;
            }
            nextNode.parent = startNode;
            nextNode.cell.x = startNode.cell.x;
            nextNode.cell.y = y;
            gMinMaxSearch.computerScore += gGameBoard[nextNode.cell.y][nextNode.cell.x];
            startNode.children[y] = minmax(nextNode, "computerturn");
        }
        // Same as above, just in reverse.  Thus we find the largest value
        //  in the '.children' array.
        result = maxVal(startNode.children, gGameOptions.rows);
        gMinMaxSearch.humanScore -= gGameBoard[startNode.cell.y][startNode.cell.x];
        if( result == -1 ) {
            // We reached a point where there are no more moves that can be
            // made so we evaluate the node as if we had reached our maximum
            // search depth.
            gMinMaxSearch.curSearchDepth--;
            result = evaluate();
            gMinMaxSearch.humanScore -= gGameBoard[startNode.cell.y][startNode.cell.x];
            gMinMaxSearch.visited[startNode.cell.y][startNode.cell.x] = gGameBoardVisited[startNode.cell.y][startNode.cell.x];
            return (result);
//			return(true);
        }
    }
    gMinMaxSearch.visited[startNode.cell.y][startNode.cell.x] = gGameBoard[startNode.cell.y][startNode.cell.x];
    gMinMaxSearch.curSearchDepth--;
    return (startNode.children[result]);
}

function rowCompletelyUsed() {
    if( gGame.selRow == -1 ) {
        return (false);
    }
    for( var i = 0; i < gGameOptions.cols; i++ ) {
        if( gGameBoardVisited[gGame.selRow][i] == false ) {
            return (false);
        }
    }
    return (true);
}

function columnCompletelyUsed() {
    if( gGame.selCol == -1 ) {
        return (false);
    }
    for( var j = 0; j < gGameOptions.rows; j++ ) {
        if( gGameBoardVisited[j][gGame.selCol] == false ) {
            return (false);
        }
    }
    return (true);
}

function validMoveExists() {
    if( gGame.state == "player1turn" ) {
        return ( !rowCompletelyUsed() );
    }
    return ( !columnCompletelyUsed() );
}

function gameOver() {
    var gameOverMsg = "Game Over!";
    if( gGameOptions.keepWL ) {
        var key = "wl" + gPlayer1.name + ":" + gPlayer2.name;
        var wl = localStorage.getItem(key);
        // If the key is not found (wl == null) then we create it.  Othwerwise
        // we update the existing win/loss record and re-store it.
        if( wl == null ) {
            wl = "1-0";
            if( gPlayer2.score > gPlayer1.score ) {
                wl = "0-1";
            }
        }
        else {
            wl = wl.split("-");
            if( gPlayer1.score > gPlayer2.score ) {
                wl[0]++;
            }
            else {
                wl[1]++;
            }
            wl = wl.join("-");
        }
        localStorage.setItem(key, wl);
        // Add the record to the game over message.
        wl = wl.split("-");
        gameOverMsg += "\n\nWins:\n" + gPlayer1.name + ": " + wl[0] + "\n" + gPlayer2.name + ": " + wl[1];
    }
    alert(gameOverMsg);
}

function nextTurn() {
    updateUI();
    if( !validMoveExists() ) {
        gameOver();
        return;
    }
    // If it is the computer's turn to go, then set the computer to thinking.
    if( isComputerTurn() ) {
        // Wait half a second to let the browser's UI update.
        setTimeout("computerMove()", 500);
    }
}

function getHumanPlayer() {
    // If the opponent is a computer then we simply return the player which is
    // not a computer player.
    if( gGameOptions.opponent == "computer" ) {
        if( !gPlayer1.isComputer ) {
            return ("gPlayer1");
        }
        return ("gPlayer2");
    }
    // If the opponent is a human then we return the player whose turn it
    // currently is.
    else {
        switch( gGame.state ) {
            case "player1turn":
                return ("gPlayer1");
            case "player2turn":
                return ("gPlayer2");
            default:
                return (null);
        }
    }
}

function getComputerPlayer() {
    if( gPlayer1.isComputer ) {
        return ("gPlayer1");
    }
    return ("gPlayer2");
}

function changePlayerName(e) {
    window[e.data.player].name = prompt("Enter the player's new name:");
    updateUI();
}

function browser_supports_html5_storage() {
    try {
        return 'localStorage' in window && window['localStorage'] != null;
    } catch( e ) {
        return (false);
    }
}

function validBoardSize(size) {
    return boardSizeChoices.indexOf(size) > -1;
}

function validComputerLevel(l) {
    return computerLevelChoices.indexOf(l) > -1;
}

function validOpponentType(o) {
    return opponentType.indexOf(o) > -1;
}

function getOptionSize() {
    return parseInt(localStorage.getItem("size"));
}

function getOptionComputerLevel() {
    return parseInt(localStorage.getItem("computerLevel"));
}

function savedOptionsExist() {
    return validBoardSize(getOptionSize()) &&
        validComputerLevel(getOptionComputerLevel()) &&
        validOpponentType(localStorage.getItem("opponent"));
}

function setOptionElements(options) {
    $("#options-board-size").val(options.rows).change();
    $("#options-computer-level").val(options.computerLevel).change();
    $("#options-opponent").val(options.opponent).change();
}

function saveGameOptions(options) {
    if( !options ) {
        options = {
            size: parseInt($("#options-board-size").val()),
            computerLevel: parseInt($("#options-computer-level").val()),
            opponent: $("#options-opponent").val()
        };
    }
    if( browser_supports_html5_storage() ) {
        try {
            localStorage.setItem("size", options.size);
            localStorage.setItem("computerLevel", options.computerLevel);
            localStorage.setItem("opponent", options.opponent);
        }
        catch( err ) {
            // IE8 gives us an object, other browsers give us a string.
            if( err.message ) {
                err = err.message;
            }
            showMsg("Error saving options: " + err, "error", 10);
        }
    }
    else {
        // Save using cookies.
        var cookie_expire_date = new Date();
        cookie_expire_date.setDate(cookie_expire_date.getDate() + 100000);
        var cookie_value = "size=" + options.size + "&computerLevel=" + options.computerLevel + "&opponent=" + options.opponent;
        document.cookie = gCookieName + "=" + encodeURI(cookie_value) + ";expires=" + cookie_expire_date.toUTCString();
    }
}

function loadGameOptions() {
    if( browser_supports_html5_storage() ) {
        try {
            gGameOptions.rows = gGameOptions.cols = getOptionSize();
            gGameOptions.computerLevel = getOptionComputerLevel();
            gGameOptions.opponent = localStorage.getItem("opponent");
        } catch( err ) {
            showMsg("Error loading saved options: " + err, "error", 10);
        }
    }
    else {
        // Load from cookie.
        var cookies = document.cookie.split(";");
        for( var i = 0; i < cookies.length; i++ ) {
            var nv = cookies[i].split("=");
            if( nv[0] == gCookieName ) {
                var arr = decodeURI(nv[1]).split("&");
                for( var a = 0; a < arr.length; a++ ) {
                    nv = arr[a].split("=");
                    switch( nv[0] ) {
                        case "size":
                            gGameOptions.rows = gGameOptions.cols = nv[1];
                            break;
                        case "computerLevel":
                            gGameOptions.computerLevel = nv[1];
                            break;
                        case "opponent":
                            gGameOptions.opponent = nv[1];
                            break;
                    }
                }
            }
        }
    }
    setOptionElements(gGameOptions);
}

function showMsg(msg, type) {
    console.log(type + ": " + msg);
}

function createEventHandlers() {
    $("#btnNewGame").click(newGame);
    $("#player1Name").click({player: "gPlayer1"}, changePlayerName);
    $("#player2Name").click({player: "gPlayer2"}, changePlayerName);
    $(document).on('click', 'table#tableGameBoard td', function() {
        // If it is the computers turn then we ignore the click.
        if( isComputerTurn() ) {
            return;
        }
        // Get the coordinates of the selected cell.
        var p = (new Point()).fromString($(this).attr("id").substring(11));
        // If the cell has already been chosen then we ignore the click.
        if( gGameBoardVisited[p.y][p.x] == true ) {
            showMsg("That square has already been selected.  Please choose a different square on the highlighted row.", "error", 5);
            return;
        }
        // Allow player1 to only select cells in the current row.  If this is
        // the first move of the game (where gGame.selRow == -1) then the
        // player can select any row they want.
        if( gGame.state == "player1turn" && p.y != gGame.selRow && gGame.selRow != -1 ) {
            showMsg("Only numbers in the highlighted row can be selected.", "error", 5);
            return;
        }
        // Allow player2 to only select cells in the current column.
        if( gGame.state == "player2turn" && p.x != gGame.selCol && gGame.selCol != -1 ) {
            showMsg("Only numbers in the highlighted column can be selected.", "error", 5);
            return;
        }
        // Mark the cell as having already been chosen.
        markVisited(p);
        // Add the value of the cell to the human's score.
        window[getHumanPlayer()].score += gGameBoard[p.y][p.x];
        // Set the state of the game.
        gGame.selRow = p.y;
        gGame.selCol = p.x;
        gGame.state = nextPlayerTurn(gGameBoard[p.y][p.x], p);
        // Move to the next player.
        nextTurn();
    });
}

$(document).ready(function() {
    $(".button-collapse").sideNav({
        edge: 'right',
        closeOnClick: true,
        draggable: false
    });

    $("#help-dialog.modal").modal({
        dismissable: true,
        opacity: 0.5
    });

    $("#options-dialog.modal").modal({
        dismissable: true,
        opacity: 0.5,
        ready: function(modal, trigger) {
        },
        complete: function() {
            saveGameOptions();
            loadGameOptions();
        }
    });

    //$("select").material_select();


    createEventHandlers();

    if( savedOptionsExist()) {
        loadGameOptions();
    } else {
        saveGameOptions({
            size: 7,
            computerLevel: 2,
            opponent: 'computer'
        });
        loadGameOptions();
    }
});
